#include "cmac.h"

int cmac(unsigned char key[], unsigned char message[], int message_len, unsigned char mac[]){
	CMAC_CTX *ctx = CMAC_CTX_new();
	if(!CMAC_Init(ctx, key, 16, EVP_aes_128_cbc(), NULL)) return 0; 
	if(!CMAC_Update(ctx, message, message_len)) return 0;  
	size_t mac_len;
	if(!CMAC_Final(ctx, mac, &mac_len)) return 0;  
	CMAC_CTX_free(ctx); 
	return 1;
}
