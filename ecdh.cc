#include "ecdh.h"

/*Elliptic Curve Diffie-Hellman function*/
unsigned char* EC_DH(int *secret_len, EC_KEY *key, const EC_POINT *pPub)
{
    int fieldSize;
    fieldSize = EC_GROUP_get_degree(EC_KEY_get0_group(key));
    *secret_len = (fieldSize + 7) / 8;
    unsigned char *secret = (unsigned char *) malloc(*secret_len);
    if (!secret)
        return NULL;
    *secret_len = ECDH_compute_key(secret, *secret_len, pPub, key, NULL);
    if(*secret_len <= 0)
    {
        OPENSSL_free(secret);
        return NULL;
    }
    return secret;
}


/*Key generation function for throwaway keys.*/
EC_KEY* gen_key(void)
{
    EC_KEY *key;

    key = EC_KEY_new_by_curve_name(NID_X9_62_prime256v1); // prime256v1
    if (key == NULL)
        return NULL;

    if (!EC_KEY_generate_key(key))
        return NULL; 

    return key;
}
