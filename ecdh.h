
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/ec.h>
#include <openssl/ecdh.h>
#include <openssl/evp.h>

#define ECC_PRV_KEY_SIZE 32
#define ECC_COORDINATE_SIZE ECC_PRV_KEY_SIZE
#define ECC_PUB_KEY_SIZE 2*ECC_COORDINATE_SIZE

EC_KEY* gen_key(void); 
unsigned char* EC_DH(int *secret_len, EC_KEY *key, const EC_POINT *pPub);





