/****************************************************************************
 *  Copyright: National ICT Australia,  2007 - 2010                         *
 *  Developed at the ATP lab, Networked Systems research theme              *
 *  Author(s): Athanassios Boulis, Yuriy Tselishchev                        *
 *  This file is distributed under the terms in the attached LICENSE file.  *
 *  If you do not find this file, copies can be found by writing to:        *
 *                                                                          *
 *      NICTA, Locked Bag 9013, Alexandria, NSW 1435, Australia             *
 *      Attention:  License Inquiry.                                        *
 *                                                                          *  
 ****************************************************************************/

#ifndef BaselineBAN_MAC_MODULE_H
#define BaselineBAN_MAC_MODULE_H
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "VirtualMac.h"
#include "BaselineMacPacket_m.h"
#include "ecdh.h"
#include "cmac.h"
#include "../../application/VirtualApplication.h"
#include "../../communication/routing/VirtualRouting.h"
#define TX_TIME(x) (phyLayerOverhead + x)*1/(1000*phyDataRate/8.0) //x are in BYTES
#define UNCONNECTED -1

// Guard time used to start RX earlier and allow less time to finish TX
#define GUARD_TIME (pastSyncIntervalNominal ? allocationSlotLength / 10.0 + extraGuardTime() : allocationSlotLength/10.0)
// Guard time used to delay the start of a transmission
#define GUARD_TX_TIME (pastSyncIntervalNominal ? extraGuardTime() : 0.0)
// We discovered that ending a TX we should really guard for 2*GT instead of 1*GT.
// We offer this option with the enhanceGuardTime parameter
#define GUARD_FACTOR (enhanceGuardTime ? 2.0 : 1.0)

#define SLEEP2TX (isRadioSleeping ? pTimeSleepToTX : 0.0)

#define MAC_SELF_ADDRESS self
#define MGMT_BUFFER_SIZE 16  // hold maximum of 16 management packets

using namespace std;

#define NONCE_SIZE 16
#define MK_SIZE  16
#define PTK_SIZE 16 
#define T1_SIZE 16 
#define T2_SIZE 8 
#define T3_SIZE 8
#define T4_SIZE 16
#define DHKey_SIZE ECC_PUB_KEY_SIZE/2
#define CMAC_SIZE 16 
#define MESSAGE_LEN 1 + 1 + NONCE_SIZE + NONCE_SIZE
#define MESSAGE_MK_LEN NONCE_SIZE+ NONCE_SIZE
#define MESSAGE_PTK_LEN 1 + 1 + NONCE_SIZE + NONCE_SIZE
#define PTK_T3_SIZE 8
#define PTK_T4_SIZE 8
#define PTK_T1_T2_SIZE 16 
#define EMPTY_ENCRYPTION_BUFFER_TIME 2
enum MacStates {
	MAC_SETUP = 1000,
	MAC_RAP = 1001,
	MAC_FREE_TX_ACCESS = 1002,
	MAC_FREE_RX_ACCESS = 1003,
	MAC_BEACON_WAIT = 1009,
	MAC_SLEEP = 1010
};

enum Timers {
	CARRIER_SENSING = 1,
	START_ATTEMPT_TX = 2,
	ACK_TIMEOUT = 3,
	START_SLEEPING = 4,
	START_SCHEDULED_TX_ACCESS = 5,
	START_SCHEDULED_RX_ACCESS = 6,
	WAKEUP_FOR_BEACON = 7,
	SYNC_INTERVAL_TIMEOUT = 8,
	SEND_BEACON = 9,
	HUB_SCHEDULED_ACCESS = 10,
	START_SETUP = 11,
	START_POSTED_ACCESS = 12,
	SEND_FUTURE_POLLS = 13,
	SEND_POLL = 14,
	INCREMENT_SLOT = 15,
	EMPTY_ENCRYPTION_BUFFER = 16
};

static int CWmin[8] = { 16, 16, 8, 8, 4, 4, 2, 1 };
static int CWmax[8] = { 64, 32, 32, 16, 16, 8, 8, 4};

struct TimerInfo {
	int NID;
	int slotsGiven;
	int endSlot;
};

struct slotAssign_t {
	int NID;
	int startSlot;
	int endSlot;
};

struct AccessSlot {
	int scheduled;
	int polled;
};

struct securitySession_t {
	int NID=0;
	uint8_t other_pub[ECC_PUB_KEY_SIZE]={0};
	unsigned char MKey[MK_SIZE]={0}; 
	unsigned char PTKey[PTK_SIZE]={0}; 
	bool isPTK_MK_established=false;
	bool isMK_established = false; 
	unsigned char my_nonce[NONCE_SIZE]={0}; 
	unsigned char other_nonce[NONCE_SIZE]={0}; 
	unsigned char t2[T2_SIZE]={0};
	unsigned char t3[T3_SIZE]={0}; 
	unsigned char my_ptk_nonce[NONCE_SIZE] = {0}; 
	unsigned char other_ptk_nonce[NONCE_SIZE] = {0}; 
	unsigned char ptk_t3[PTK_T3_SIZE]={0};
	unsigned char ptk_t4[PTK_T4_SIZE]={0}; 
	
};


class BaselineBANMac : public VirtualMac {
	private:
	EC_KEY *key; 
	uint8_t pub[ECC_PUB_KEY_SIZE] = {0};
  	uint8_t prv[ECC_PRV_KEY_SIZE] = {0};
  	bool crypto_initialized= false; 
	bool isHub;
	int connectedHID;
	int connectedNID;
	int unconnectedNID;

	double allocationSlotLength;
	int RAP1Length;
	int beaconPeriodLength;

	int scheduledTxAccessStart;
	int scheduledTxAccessEnd;
	int scheduledAccessLength;
	int scheduledAccessPeriod;

	int scheduledRxAccessStart;
	int scheduledRxAccessEnd;

	int polledAccessEnd;
	int postedAccessEnd;

	double pTIFS;
	int phyLayerOverhead;
	double phyDataRate;
	int priority;
	double contentionSlotLength;

	int maxPacketTries;
	int currentPacketTransmissions;
	int currentPacketCSFails;

	int CW;
	bool CWdouble;
	int backoffCounter;

	MacStates macState;

	bool pastSyncIntervalNominal;
	simtime_t syncIntervalAdditionalStart;

	BaselineMacPacket *packetToBeSent;
	simtime_t endTime; // the time when our right to TX ends. Covers RAP, scheduled and polled access
	simtime_t frameStartTime; // time the frame starts (when we receive the beacon - beacon TX time)

	double SInominal;
	double mClockAccuracy;
	bool enhanceGuardTime;
	bool enhanceMoreData;
	bool pollingEnabled;
	bool naivePollingScheme;
	bool isRadioSleeping;
	double pTimeSleepToTX;
	bool enableRAP;

	bool waitingForACK;
	bool futureAttemptToTX;
	bool attemptingToTX;

	bool isPollPeriod;

	// a buffer to store Management packets that require ack and possible reTX
	// these packets are treated like data packets, but with higher priority
	queue <BaselineMacPacket *> MgmtBuffer;

	map <int, securitySession_t*> securityAssociationMap;
	multimap <int, cPacket*> EncryptionBuffer;// Holds packets that need to be encrypted after security session is established
	
	// the rest of the variables are hub-specific. They get allocated/used only for the hub
	int currentSlot;
	bool sendIAckPoll;
	int currentFirstFreeSlot;
	int currentFreeConnectedNID;
	int nextFuturePollSlot;
	AccessSlot *lastTxAccessSlot;
	int *reqToSendMoreData;
	queue <TimerInfo> hubPollTimers;
	map <int, slotAssign_t> slotAssignmentMap;
	protected:
	void setDestinationNID(BaselineMacPacket *pkt, int dst_mac);

	int addToEncryptionBuffer(cPacket *pkt, int dst);
	void encryptData(cPacket *pkt, int dst);
	bool empty_encryption_buffer();
	bool compareTvalue_ptk(unsigned char *t, BaselinePTKPacket *pkt, int len);
	int buildMKMessage(unsigned char message[], unsigned char *nonce1, unsigned char *nonce2, int total_size);
	int buildMessage(unsigned char *message, int v1, int v2, unsigned char *nonce1, unsigned char *nonce2, int total_size);

	BaselinePTKPacket* createPTKPacket(int dest, int ptkSeqNum);
	void printPTK(unsigned char *ptk);

	void clear_ptk_kmac(BaselinePTKPacket *pkt); 
	void calculatePTKTvalues(int dest, bool isInit);

	bool isPTKMKEstablished(int dest);
	bool isMKEstablished(int dest);


	void printMK(unsigned char *mk);
	void set_tvalue(BaselineSecurityAssociationPacket * pkt, unsigned char *t);
	bool compareTvalue(unsigned char *t, BaselineSecurityAssociationPacket *pkt, int len);
	void calculateMKKey(int dest, bool isInit);
	

	int buildT3Message(int dest, unsigned char *message);
	int buildT2Message(int dest, unsigned char *message);


	unsigned char* generate_dh_key(int dest);

	void calculateTvalues(int dest);
	void printDHKey(unsigned char *DHKey, int len);
	
	void printPubKey(EC_KEY *key);
	void printOtherPubKey(unsigned char *pub);

	void printNonce(unsigned char *nonce);
	BaselineSecurityAssociationPacket* createPacket(int dest, int assoSeqNum);
	void gen_nonce(unsigned char *nonce, int size);
	void save_other_nonce(securitySession_t *session,BaselineSecurityAssociationPacket *pkt);
	void set_nonce(BaselineSecurityAssociationPacket * pkt, unsigned char *nonce);
	void set_ptk_nonce(BaselinePTKPacket *pkt, unsigned char *nonce); //Set PTK Nonce Function
	void set_ptk_kmac(BaselinePTKPacket *pkt, unsigned char *ptk_kmac);  //Set PTK KMAC Function
	void save_other_ptk_nonce(securitySession_t *session,BaselinePTKPacket *pkt); //Save PTK Packet's nonce
	securitySession_t* createSecuritySession(int dest);
	securitySession_t* findSession(int id);
	void disassociate(int dest);
	void save_other_public_key(securitySession_t *session, BaselineSecurityAssociationPacket *pkt);
	
	void set_public_key(BaselineSecurityAssociationPacket *pkt);
	void sendSecAssoMessage1(int dest);
	void acceptSecAssoMessage1(BaselineSecurityAssociationPacket *pkt);

	void sendSecAssoMessage2(BaselineSecurityAssociationPacket *pkt);
	void acceptSecAssoMessage2(BaselineSecurityAssociationPacket *pkt);

	void sendSecAssoMessage3(int dest);
	void acceptSecAssoMessage3(BaselineSecurityAssociationPacket *pkt);

	void sendSecAssoMessage4(int dest);
	void acceptSecAssoMessage4(BaselineSecurityAssociationPacket *pkt);

	void sendPTKMessage1(int dest);
	void acceptPTKMessage1(BaselinePTKPacket *pkt);

	void sendPTKMessage2(int dest);
	void acceptPTKMessage2(BaselinePTKPacket *pkt);

	void sendPTKMessage3(int dest);
	void acceptPTKMessage3(BaselinePTKPacket *pkt);

	void crypto_init(void);
	void startup();
	void timerFiredCallback(int);
	void fromNetworkLayer(cPacket*, int);
	void fromRadioLayer(cPacket*,double,double);
	void finishSpecific();
	bool isPacketForMe(BaselineMacPacket *pkt);
	simtime_t extraGuardTime();
	void setHeaderFields(BaselineMacPacket *pkt, AcknowledgementPolicy_type ackPolicy, Frame_type frameType, Frame_subtype frameSubtype);
	void attemptTxInRAP();
	void attemptTX();
	bool canFitTx();
	void sendPacket();
	void handlePoll(BaselineMacPacket *pkt);
	void handlePost(BaselineMacPacket *pkt);
	void handleMoreDataAtHub(BaselineMacPacket *pkt);
	simtime_t timeToNextBeacon(simtime_t interval, int index, int phase);

};

#endif // BaselineBAN_MAC_MODULE_H
